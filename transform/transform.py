import logging

from analysis.analysis import classify
from util import read_sql_files


def transform(db, country, rules=True):
    read_sql_files(country, rules=rules)
    for basename, content in read_sql_files(country).items():
        logging.info(f'Ejecutando query {basename}')
        db.query(content)
    db.commit()


def database_creation(db, country):
    for basename, content in read_sql_files(country, 'migrations').items():
        db.query(content)
    db.commit()
