insert into colombia.notification(ocid, characteristics, creation_date)
SELECT p.ocid, to_jsonb('covid'::text), now()
from colombia.procurement p
left join colombia.notification n on p.ocid = n.ocid where n.ocid is null
and (p.tender_description ilike '%covid%' or p.tender_title ilike '%covid%');

delete from colombia.award where data_id in (
select a.data_id from colombia.procurement a
join colombia.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from colombia.award_items where data_id in (
select a.data_id from colombia.procurement a
join colombia.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from colombia.tender_items where data_id in (
select a.data_id from colombia.procurement a
join colombia.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from colombia.contract where data_id in (
select a.data_id from colombia.procurement a
join colombia.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from colombia.parties where data_id in (
select a.data_id from colombia.procurement a
join colombia.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from colombia.procurement where data_id in (
select a.data_id from colombia.procurement a
join colombia.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from colombia.data;