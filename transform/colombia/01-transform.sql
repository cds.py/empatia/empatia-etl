INSERT INTO colombia.procurement (release_date, ocid, data_id, tender_id, tender_amount, budget_amount, budget_currency,
                                  tender_currency, tender_tenderperiod_start_date, tender_procurementmethoddetails,
                                  tender_procurementmethod, buyer_name, buyer_id,
                                  tender_bidopening_date, tender_status, tender_title, tender_description,
                                  tender_mainprocurementcategory,
                                  tender_numberoftenderers, framework_agreement, analyzed, number_of_awards,
                                  process_statistics,
                                  process_url) (
    SELECT distinct (data ->> 'date')::timestamp                                                 as release_date,
                    r.ocid,
                    data_id                                                                      as data_id,
                    data -> 'tender' ->> 'id'                                                    as tender_id,

                    coalesce((data -> 'tender' -> 'value' -> 'amount' ->> '$numberLong')::numeric,
                             (data -> 'tender' -> 'value' ->> 'amount')::numeric)                as tender_amount,
                    coalesce((data -> 'planning' -> 'budget' -> 'amount' -> 'amount' ->> '$numberLong')::numeric,
                             (data -> 'planning' -> 'budget' -> 'amount' ->> 'amount')::numeric) as budget_amount,
                    data -> 'planning' -> 'budget' -> 'amount' ->> 'currency'                    as budget_currency,
                    data -> 'tender' -> 'value' ->> 'currency'                                   as tender_currency,
                    (data -> 'tender' -> 'tenderPeriod' ->> 'startDate')::timestamp              as tender_tenderperiod_start_date,
                    (data -> 'tender' ->> 'procurementMethodDetails')                            as tender_procurementMethodDetails,
                    (data -> 'tender' ->> 'procurementMethod')                                   as tender_procurementMethod,
                    (data -> 'buyer' ->> 'name')                                                 as buyer_name,
                    (data -> 'buyer' ->> 'id')                                                   as buyer_id,
                    (data -> 'tender' -> 'bidOpening' ->> 'date')::timestamp                     as tender_bidopening_date,
                    data -> 'tender' ->> 'status'                                                as tender_status,
                    data -> 'tender' ->> 'title'                                                 as tender_title,
                    data -> 'tender' ->> 'description'                                           as tender_description,
                    data -> 'tender' ->> 'mainProcurementCategory'                               as tender_mainprocurementCategory,
                    data -> 'tender' ->> 'numberOfTenderers'                                     as tender_numberoftenderers,
                    case
                        when data -> 'tender' ->> 'procurementMethodDetails' = 'Selección abreviada por Acuerdo Marco'
                            then true end                                                        as framework_agreement,
                    false                                                                        as analyzed,
                    COALESCE(jsonb_array_length(data -> 'awards'), 0)                            as number_of_awards,
                    data -> 'processStatistics'                                                  as process_statistics,
                    COALESCE(data ->> 'uri',
                             'https://www.contratos.gov.co/consultas/detalleProceso.do?numConstancia=' ||
                             (data -> 'tender' ->> 'id'))
                                                                                                 as process_url
    FROM (
             SELECT data.id as data_id,
                    data,
                    ocid
             FROM colombia.data
         ) AS r
    order by release_date desc);


INSERT INTO colombia.parties (ocid, data_id, party_id, name, contact_point_email, contact_point_name,
                              contact_point_telephone,
                              contact_point_fax, contact_point_url, roles, address_contry, address_locality,
                              address_region,
                              address_street, additional_contact_point_email, additional_contact_point_name,
                              additional_contact_point_telephone) (
    select r.ocid,
           r.id,
           p ->> 'id'                                          as party_id,
           p ->> 'name'                                        as name,
           p -> 'contactPoint' ->> 'email'                     as contact_point_email,
           p -> 'contactPoint' ->> 'name'                      as contact_point_name,
           p -> 'contactPoint' ->> 'telephone'                 as contact_point_telephone,
           p -> 'contactPoint' ->> 'faxNumber'                 as contact_point_fax,
           p -> 'contactPoint' ->> 'url'                       as contact_point_url,
           p -> 'roles'                                        as roles,
           p -> 'address' ->> 'countryName'                    as address_contry,
           p -> 'address' ->> 'locality'                       as address_locality,
           p -> 'address' ->> 'region'                         as address_region,
           p -> 'address' ->> 'streetAddress'                  as address_street,
           p -> 'additionalContactPoints' -> 0 ->> 'email'     as additional_contact_point_email,
           p -> 'additionalContactPoints' -> 0 ->> 'name'      as additional_contact_point_name,
           p -> 'additionalContactPoints' -> 0 ->> 'telephone' as additional_contact_point_telephone

    from colombia.data as r
             CROSS JOIN jsonb_array_elements(data -> 'parties') p
);


INSERT INTO colombia.award (ocid, data_id, award_id, award_description, award_title, date, contract_period_start_date,
                            contract_period_duration_in_days, amount, currency, status, supplier_id, supplier_name) (
    select distinct r.ocid,
                    r.id,
                    a ->> 'id'                                            as award_id,
                    a ->> 'description'                                   as award_description,
                    a ->> 'title'                                         as award_title,
                    (a ->> 'date')::timestamp                             as date,
                    (a -> 'contractPeriod' ->> 'startDate')::timestamp    as contract_period_start_date,
                    (a -> 'contractPeriod' ->> 'durationInDays')::numeric as contract_period_duration_in_days,
                    coalesce((a -> 'value' -> 'amount' ->> '$numberLong')::numeric,
                             (a -> 'value' ->> 'amount')::numeric)        as amount,
                    a -> 'value' ->> 'currency'                           as currency,
                    a ->> 'status'                                        as status,
                    a -> 'suppliers' -> 0 ->> 'id'                        as supplier_id,
                    a -> 'suppliers' -> 0 ->> 'name'                      as supplier_name
    from colombia.data as r
             CROSS JOIN jsonb_array_elements(data -> 'awards') a
);

INSERT INTO colombia.contract (ocid, data_id, contract_id, award_id, description, date_signed, amount, currency, status,
                               duration_in_days, start_date, end_date, transactions, amendments) (
    select distinct r.ocid,
                    r.id,
                    a ->> 'id'                                     as contract_id,
                    a ->> 'awardID'                                as award_id,
                    a ->> 'description'                            as description,
                    (a ->> 'dateSigned')::timestamp                as date_signed,
                    coalesce((a -> 'value' -> 'amount' ->> '$numberLong')::numeric,
                             (a -> 'value' ->> 'amount')::numeric) as amount,
                    a -> 'value' ->> 'currency'                    as currency,
                    a ->> 'statusDetails'                          as status,
                    (a -> 'period' ->> 'durationInDays')::numeric  as duration_in_days,
                    (a -> 'period' ->> 'startDate')::timestamp     as start_date,
                    (a -> 'period' ->> 'endDate')::timestamp       as end_date,
                    a -> 'implementation' -> 'transactions'        as transactions,
                    a -> 'amendments'                              as amendments
    from colombia.data as r
             CROSS JOIN jsonb_array_elements(data -> 'contracts') a
);

insert into colombia.tender_items (ocid, data_id, item_id, description, classification_id, classification_description) (
    select distinct r.ocid,
                    r.id,
                    a ->> 'id'                              as item_id,
                    a ->> 'description'                     as description,
                    a -> 'classification' ->> 'id'          as classification_id,
                    a -> 'classification' ->> 'description' as classification_description
    from colombia.data as r
             CROSS JOIN jsonb_array_elements(data -> 'tender' -> 'items') a
);

insert into colombia.award_items (ocid, data_id, award_id, item_id, description, classification_id,
                                  classification_description, quantity, unit_name) (
    select distinct r.ocid,
                    r.id,
                    aa ->> 'id'                                                                         as award_id,
                    a ->> 'id'                                                                          as item_id,
                    a ->> 'description'                                                                 as description,
                    a -> 'classification' ->> 'id'                                                      as classification_id,
                    a -> 'classification' ->> 'description'                                             as classification_description,
                    coalesce((a -> 'quantity' ->> '$numberLong')::numeric, (a ->> 'quantity')::numeric) as quantity,
                    (a -> 'unit' ->> 'name')                                                            as unit_name
    from colombia.data as r
             CROSS JOIN jsonb_array_elements(data -> 'awards') aa
             CROSS JOIN jsonb_array_elements(aa -> 'items') a
);

insert into colombia.contract_items (ocid, data_id, contract_id, item_id, description, classification_id,
                                     classification_description, quantity, unit_name) (
    select distinct r.ocid,
                    r.id,
                    aa ->> 'id'                                                                         as contract_id,
                    a ->> 'id'                                                                          as item_id,
                    a ->> 'description'                                                                 as description,
                    a -> 'classification' ->> 'id'                                                      as classification_id,
                    a -> 'classification' ->> 'description'                                             as classification_description,
                    coalesce((a -> 'quantity' ->> '$numberLong')::numeric, (a ->> 'quantity')::numeric) as quantity,
                    (a -> 'unit' ->> 'name')                                                            as unit_name
    from colombia.data as r
             CROSS JOIN jsonb_array_elements(data -> 'contracts') aa
             CROSS JOIN jsonb_array_elements(aa -> 'items') a
);

create table if not exists colombia.parametros as (
    SELECT key, type, ROW_NUMBER() OVER () AS value
    FROM (select distinct tender_mainprocurementcategory as key, 'procurement_category' as type
          from colombia.procurement p
                   join colombia.tender_items i on p.data_id = i.data_id and i.classification_id is not null
          where framework_agreement is null
            and buyer_id is not null
            and buyer_id != ''
            and tender_amount > 0
            and tender_amount is not null
            and budget_amount is not null
            and (tender_currency is null or tender_currency = 'COP')
          union
          select distinct tender_procurementmethoddetails as key, 'procurement_method' as type
          from colombia.procurement p
                   join colombia.tender_items i on p.data_id = i.data_id and i.classification_id is not null
          where framework_agreement is null
            and buyer_id is not null
            and buyer_id != ''
            and tender_amount > 0
            and tender_amount is not null
            and budget_amount is not null
            and (tender_currency is null or tender_currency = 'COP')
         ) key
);
alter table colombia.parametros add column if not exists code text;
update colombia.parametros set code = key;
update colombia.parametros
set value = 6
where key = 'Concurso de méritos abierto';
update colombia.parametros
set value = 3
where key = 'Licitación obra pública';
update colombia.parametros
set value = 8
where key = 'Licitación pública';

update colombia.parametros
set code = 'Concurso de méritos abierto'
where value = 6;
update colombia.parametros
set code = 'Licitación obra pública'
where value = 3;
update colombia.parametros
set code = 'Licitación pública'
where value = 8;