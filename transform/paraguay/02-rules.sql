insert into paraguay.notification(ocid, characteristics, creation_date)
SELECT distinct p.ocid, p.characteristics, now()
from paraguay.procurement p
left join paraguay.notification n on p.ocid = n.ocid where n.ocid is null
and p.characteristics ? 'covid_19';

-- insert into paraguay.notification(ocid, event, creation_date, parent_id)
-- select
--        b.ocid,
-- case when b.tender_status != a.tender_status then concat('CONVOCATORIA PASO A ', b.tender_status)
-- end, now(), n.id
-- from paraguay.procurement A
--     join paraguay.procurement B on a.ocid = b.ocid and b.id > a.id and (b.tender_status != a.tender_status)
--     join paraguay.notification n on n.ocid = a.ocid and n.notification_date = (select
--                                                                                       min(notification_date)
--         from paraguay.notification where ocid = n.ocid and n.status = 'NOTIFIED');

