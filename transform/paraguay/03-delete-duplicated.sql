delete from paraguay.award where data_id in (
select a.data_id from paraguay.procurement a
join paraguay.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from paraguay.award_items where data_id in (
select a.data_id from paraguay.procurement a
join paraguay.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from paraguay.tender_items where data_id in (
select a.data_id from paraguay.procurement a
join paraguay.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from paraguay.contract where data_id in (
select a.data_id from paraguay.procurement a
join paraguay.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from paraguay.parties where data_id in (
select a.data_id from paraguay.procurement a
join paraguay.procurement b on a.ocid = b.ocid and a.id < b.id);

delete from paraguay.procurement where data_id in (
select a.data_id from paraguay.procurement a
join paraguay.procurement b on a.ocid = b.ocid and a.id < b.id);

 -- delete from paraguay.data;