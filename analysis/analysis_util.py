from sklearn.tree import DecisionTreeClassifier

QUARTIL_1 = {'colombia': -0.13, 'paraguay': -0.08}

categorias = {'paraguay': [
    "Categoría Textiles, vestuarios y calzados",
    "Categoría Equipos, Productos e instrumentales Médicos y de Laboratorio. Servicios asistenciales de salud",
    "Categoría Muebles y Enseres",
    "Categoría Construcción, Restauración, Reconstrucción o Remodelación y Reparación de Inmuebles",
    "Categoría Materiales e insumos eléctricos, metálicos y no metálicos, Plásticos, cauchos. Repuestos, herramientas, cámaras y cubiertas.",
    "Categoría Capacitaciones y Adiestramientos",
    "Categoría Pasajes y Transportes",
    "Categoría Equipos, accesorios y programas computacionales, de oficina, educativos, de imprenta, de comunicación y señalamiento",
    "Categoría Utiles de oficina, Productos de papel y cartón e impresos",
    "Categoría Seguros",
    "Categoría Elementos e insumos de limpieza",
    "Categoría Productos Alimenticios",
    "Categoría Utensilios de cocina y comedor, Productos de Porcelana, vidrio y loza",
    "Categoría Publicidad y Propaganda",
    "Categoría Combustibles y Lubricantes",
    "Categoría Equipos Militares y de Seguridad.Servicio de Seguridad y Vigilancia",
    "Categoría Productos quimicos",
    "Categoría Servicios Técnicos",
    "Categoría Consultorías, Asesorías e Investigaciones. Estudios y Proyectos de inversión",
    "Categoría Maquinarias, Equipos y herramientas mayores - Equipos de transporte",
    "Categoría Adquisición y Locación de inmuebles.Alquiler de muebles",
    "Categoría Minerales",
    "Categoría Servicios de ceremonial, gastronomico y funerarios",
    "Categoría Servicios de Limpiezas, Mantenimientos y reparaciones menores y mayores de Instalaciones, Maquinarias y Vehículos",
    "Categoría Bienes e insumos agropecuario y forestal"
], 'colombia': ["works",
                "services",
                "goods"]}

modalidades = {'paraguay': [
    "Locación de Inmuebles",
    "Concurso de Ofertas",
    "Selección basada en la calidad y el costo",
    "Contratación por Excepción",
    "Licitación Pública Internacional",
    "Selección basada en la calidad",
    "Selección cuando el presupuesto es fijo",
    "Licitación Pública Nacional",
    "Acuerdo Internacional",
    "Concurso de Ofertas",
    "Contratación Directa",
    "Selección basada en las calificaciones de los consultores",
    "Acuerdo Nacional",
    "Selección basada en el menor costo",
    "Selección sobre la base la comparación de las calificaciones"
], 'colombia': [

    "Selección Abreviada de Menor Cuantía (Ley 1150 de 2007)",
    "Selección Abreviada del literal h del numeral 2 del artículo 2 de la Ley 1150 de 2007",
    "Selección Abreviada de Menor Cuantía",
    "Contratación Directa (Ley 1150 de 2007)",
    "Enajenación de bienes con sobre cerrado",
    "Selección Abreviada servicios de Salud",
    "Contratación Directa (con ofertas)",
    "Asociación Público Privada",
    "Licitación pública",
    "Enajenación de bienes con subasta",
    "Licitación obra pública",
    "Contratación régimen especial (con ofertas)",
    "Contratación directa",
    "Contratos y convenios con más de dos partes",
    "Contratación Mínima Cuantía",
    "Mínima cuantía",
    "Contratación régimen especial",
    "Concurso de Méritos con Lista Corta",
    "Selección abreviada subasta inversa",
    "Régimen Especial",
    "Seleccion Abreviada Menor Cuantia Sin Manifestacion Interes",
    "Subasta",
    "Concurso de méritos abierto"

]}

formas_adjudicacion = {'paraguay': [
    "Adjudicación Por Total",
    "Adjudicación Combinado",
    "Adjudicación Por Lote",
    "Adjudicación Por Item"
], 'colombia': []}
decision_tree_drop_columns = {'paraguay': ['ocid', 'procurement_method', 'category', 'buyer_id', 'award_criteria'],
                              'colombia': ['ocid', 'procurement_method', 'category', 'buyer_id']}

variables_categoricas = {'paraguay': ['procurement_method_desc', 'category_desc',
                                      'award_criteria_desc'], 'colombia': ['procurement_method_desc', 'category_desc']}

llamados_columnas_en_orden = {'paraguay': ['ocid',
                                           'tender_amount',
                                           'tender_period',
                                           'tender_enquiry_period',
                                           'tender_period_enquiry',
                                           'award_criteria',
                                           'procurement_method',
                                           'category',
                                           'buyer_id',
                                           'award_criteria_desc',
                                           'category_desc',
                                           'procurement_method_desc'],
                              'colombia': ['ocid',
                                           'tender_amount',
                                           'tender_month',
                                           'procurement_method',
                                           'category',
                                           'item_classification',
                                           'buyer_id',
                                           'category_desc',
                                           'procurement_method_desc']}


def score_a_nominal(row, country):
    if row['score'] > 0:
        return 0
    elif QUARTIL_1[country] <= row['score'] <= 0:
        return 1
    else:
        return 2


def nominal_a_score(score, country):
    if score > 0:
        return 'green'
    elif QUARTIL_1[country] <= score <= 0:
        return 'yellow'
    else:
        return 'red'


def get_marca(marca_label, marca_row):
    return 1 if marca_label in str(marca_row) else 0


def prepare_decision_tree_data(data, country):
    columnas_categoricas = categorias[country] + modalidades[country] + formas_adjudicacion[country] + ['score_label']
    llamado_columnas = llamados_columnas_en_orden[country] + columnas_categoricas
    llamado_columnas = [item for item in llamado_columnas if item not in decision_tree_drop_columns[country]]
    data['score_label'] = data.apply(lambda row: score_a_nominal(row, country), axis=1)
    data = data.drop(['score'], axis=1)

    # Con esto se convierte cada una de estos valores categóricos
    # en una columna para poder interpretarlo correctamente en el
    # árbol de decisión. Se crea una columna por marca, categoría, modalidad y forma de adjudicacion posible.
    # Si el valor corresponde a la columna entonces se completa con 1 si no con 0
    for categoria in categorias[country]:
        data[categoria] = data.apply(lambda row: get_marca(categoria, row['category_desc']), axis=1)
    for modalidad in modalidades[country]:
        data[modalidad] = data.apply(lambda row: get_marca(modalidad, row['procurement_method_desc']), axis=1)
    for forma in formas_adjudicacion[country]:
        data[forma] = data.apply(lambda row: get_marca(forma, row['award_criteria_desc']), axis=1)

    # Luego se borran las columnas numéricas que representan estos valores ca

    data = data[llamado_columnas]
    data = data.drop(decision_tree_drop_columns[country] + variables_categoricas[country], axis=1, errors='ignore')
    return data


def get_decision_path(data, estimator, country):
    data = data.drop(['slug'], axis=1, errors='ignore')
    dt_data = prepare_decision_tree_data(data, country)
    x_train = dt_data.drop(['score_label'], axis=1).to_numpy()
    column_names = list(dt_data.columns)
    feature = estimator.tree_.feature
    node_indicator = estimator.decision_path(x_train)
    leave_id = estimator.apply(x_train)
    sample_id = 0
    node_index = node_indicator.indices[node_indicator.indptr[sample_id]:node_indicator.indptr[sample_id + 1]]
    regla = ""
    check = []
    for node_id in node_index:
        if leave_id[sample_id] == node_id:
            continue
        if column_names[feature[node_id]] not in check:
            valor = f'{int(x_train[sample_id, feature[node_id]]):,}'.replace(',', '.')
            if column_names[feature[node_id]] in modalidades[country]:
                regla = regla + f'El proceso es {data.iloc[0]["procurement_method_desc"]}'
                check.extend(modalidades[country])
            if column_names[feature[node_id]] in categorias[country]:
                categoria = data.iloc[0]["category_desc"]
                if categoria == 'goods':
                    categoria = 'Bienes'
                if categoria == 'services':
                    categoria = 'Servicios'
                if categoria == 'works':
                    categoria = 'Obras'
                regla = regla + f', el proceso es {categoria}'
                check.extend(categorias[country])
            if column_names[feature[node_id]] in formas_adjudicacion[country]:
                regla = regla + f', el proceso es {data.iloc[0]["award_criteria_desc"]}'
                check.extend(formas_adjudicacion[country])
            if column_names[feature[node_id]] == 'tender_amount':
                moneda = 'GS' if country == 'paraguay' else 'COL'
                regla = regla + f', el monto referencial es {valor} {moneda}'
            if column_names[feature[node_id]] == 'tender_enquiry_period':
                regla = regla + f', el periodo de consultas es de {int(x_train[sample_id, feature[node_id]])} dias'
            if column_names[feature[node_id]] == 'tender_period':
                regla = regla + f', el periodo de envio de ofertas es de {int(x_train[sample_id, feature[node_id]])} dias'
            if column_names[feature[node_id]] == 'tender_period_enquiry':
                regla = regla + f', el periodo entre el fin de licitación y el fin de consultas es de {int(x_train[sample_id, feature[node_id]])} dias'
        check.append(column_names[feature[node_id]])
    return regla


def prediction_to_ret(data, a_ret, clf, decision_tree, country):
    data_modalidades = data['procurement_method_desc']
    data_categorias = data['category_desc']
    if country == 'paraguay':
        data_formas_adjudicacion = data['award_criteria_desc']

    # Las quitamos para el IsolationForest
    data = data.drop(variables_categoricas[country] + ['ocid', 'score', 'score_label'], axis=1, errors="ignore")
    scores = clf.decision_function(data)[0]

    data['procurement_method_desc'] = data_modalidades
    data['category_desc'] = data_categorias
    if country == 'paraguay':
        data['award_criteria_desc'] = data_formas_adjudicacion
    data['score'] = scores
    a_ret['score'] = scores

    a_ret['rules'] = get_decision_path(data, decision_tree, country)
    a_ret['risk'] = nominal_a_score(scores, country)
    return a_ret
