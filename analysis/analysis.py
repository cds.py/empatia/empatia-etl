import json
import os
import logging
import pandas as pd
from joblib import load

from analysis.analysis_util import prediction_to_ret


def classify(db, country):
    logging.info("Iniciando clasificacion")
    base_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), country)
    # buscamos todos los procesos que no hayan sido analizados
    with open(os.path.join(base_path, 'tender.sql'), 'r') as query:
        results = pd.read_sql_query(query.read().format(), db.engine)
    logging.info(f"Encontrados {len(results)} procesos para {country}")
    if results.empty:
        return


    # quitamos las variables categoricas
    categoricas = ['procurement_method_desc', 'category_desc', 'award_criteria_desc', 'ocid']
    if country == 'colombia':
        categoricas.remove('award_criteria_desc')
    to_predict = results.drop(columns=categoricas, axis=1)
    clf = load(os.path.join(base_path, 'tender.joblib'))
    prediction = clf.predict(to_predict)
    # aqui vemos los scores obtenidos
    scores = clf.decision_function(to_predict)
    predictions = pd.DataFrame()
    predictions["predicted_class"] = prediction
    predictions["score"] = scores
    results['score'] = scores
    predictions["slug"] = results["ocid"]

    # obtenemos el arbol de decision para interpretar los resultados
    decision_tree = load(os.path.join(base_path, 'decision_tree.joblib'))

    # calculamos el resultado para cada fila
    for index, row in results.iterrows():
        row = row.to_frame().T
        ocid = row['ocid'].item()
        a_ret = {'ocid': ocid}
        pred = prediction_to_ret(row, a_ret, clf, decision_tree, country)
        # si es riego es amarillo o rojo entonces insertamos en la tabla notificación
        if pred['risk'] != 'green':
            data = json.dumps(pred)

            db.query(f"INSERT INTO {country}.notification(ocid, rules) VALUES (:ocid, :rules)", ocid=ocid, rules=data)

        # marcamos como analizado
        db.query(f"UPDATE {country}.procurement set analyzed = TRUE where ocid = '{ocid}'")
    db.commit()
