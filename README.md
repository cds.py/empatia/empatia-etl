# Empatia ETL

Proyecto de ETL de datos de contrataciones públicas basado en `kingfisher collect <https://kingfisher-collect.readthedocs.io/>`__.

## Requerimientos

- Python 3 y/o docker y docker-compose
- Pip
- virtualenv

### Levantar locamente con virtualenv

- virtualenv ve -p python3
- source ve/bin/activate
- pip install -r requirements.txt

Configurar la ruta a la base de datos en settings.py

### Levantar localmente con docker

- Hacer una copia del archivo .env_sample y llamarlo .env, seteando los valores de las variables de entorno
- docker-compose build
- docker-compose up -d

Se creará una base de datos con las tablas a ser cargadas, un schema por cada país (actualmente paraguay), y se correrá
un scheduler python cada cierta cantidad de horas configuradas en EMPATIA_ETL_SCHEDULER_HOURS_PARAGUAY y
EMPATIA_ETL_SCHEDULER_HOURS_COLOMBIA

## Desarrollo
El ETL ejecuta los pipelines de paraguay y colombia, pasandole el parametro from_date segun la última fecha de corrida
guardada en la tabla "procurement" de cada schema. La primera vez esta fecha esta vacia y por defecto se traen todos los
procesos de 7 dias atrás. Los datos en json son guardados temporalmente en la tabla data, y luego, el proceso de
transformacion del ETL ejecuta los sqls en transform/pais para convertir estos jsons en tablas postgres.

También se encuentra en esta carpeta los sql para cargar la tabla "notification" en la cual se almacenan los procesos
que cumplan con ciertas reglas definidas en el sql para ser notificadas por el módulo de comunicación (bot de tuiter)-
Entonces para:
- Agregar una nueva regla: editar el archivo rules.sql del país que queramos
- Agregar una nueva columna: Editar los archivos transform.sql y también la definición de las tablas en la carpeta
  migrations

Así también luego de terminar la transformación, correr las reglas definidas en los sqls, el ETL también ejecuta los
modelos de clasificación e interpretación que se encuentran definidos en la carpeta analysis. Estos modelos provienen
del módulo de entrenamiento.

Si al realizar la clasificación de los procesos, alguno arroja que es anómalo o muy anómalo (determinado por el score obtenido
donde valores cercanos a -1 son más anomalos y los más cercanos a 1 son normales) estos son insertados en la tabla notificacion,
con el score, el riesgo (red o yellow) y la posible regla que hizo que el proceso sea anómalo, arrojado por un árbol de decisión
que se ejecuta sobre el resultado del modelo de clasificación. La tabla de notificación se queda cargada con las columnas en el
siguiente formato:

```javascript
ocid: ocds-k50g02-CO1.BDOS.1057262 (ocid del proceso)
status: PENDING
rules: {"ocid": "ocds-k50g02-CO1.BDOS.1057262", "risk": "yellow", "rules": "El proceso es Mínima cuantía, el monto referencial es 3.000.000 COL, el proceso es Servicios", "score": -0.062455957136823614} (jsonb)
creation_date: fecha de creacion
```

Para tuitear con información adicional del proceso se puede utilizar la columna ocid para realizar join con la tabla procurement y obtener
información adicional. El texto a tuitear se puede obtener de la columna `rules->>'rules'`