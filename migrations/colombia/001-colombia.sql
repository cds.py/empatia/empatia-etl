
create schema if not exists colombia;

create table if not exists colombia.data
(
    data       jsonb,
    release_id text,
    ocid       text,
    id         bigserial not null
);

create index if not exists ix_data_1a2253436964eea9
    on colombia.data (release_id);

create index if not exists ix_data_fbe00273b81f8a04
    on colombia.data (release_id, ocid);

create table if not exists colombia.procurement
(
    release_date                    timestamp,
    ocid                            text,
    data_id                         bigint,
    tender_id                       text,
    tender_amount                   numeric,
    budget_amount                   numeric,
    budget_currency                 text,
    tender_currency                 text,
    tender_tenderperiod_start_date  timestamp,
    tender_procurementmethoddetails text,
    tender_procurementmethod        text,
    buyer_name                      text,
    buyer_id                        text,
    tender_bidopening_date          timestamp,
    tender_status                   text,
    tender_title                    text,
    tender_description              text,
    tender_mainprocurementcategory  text,
    tender_numberoftenderers        text,
    framework_agreement             boolean,
    analyzed                        boolean,
    number_of_awards                integer,
    process_statistics              jsonb,
    id                              bigserial not null
        constraint procurement_pk
            primary key
);

alter table colombia.procurement add column if not exists process_url text;

create table if not exists colombia.parties
(
    ocid                               text,
    data_id                            bigint,
    party_id                           text,
    name                               text,
    contact_point_email                text,
    contact_point_name                 text,
    contact_point_telephone            text,
    contact_point_fax                  text,
    contact_point_url                  text,
    roles                              jsonb,
    address_contry                     text,
    address_locality                   text,
    address_region                     text,
    address_street                     text,
    additional_contact_point_email     text,
    additional_contact_point_name      text,
    additional_contact_point_telephone text,
    id                                 bigserial not null
        constraint parties_pk
            primary key
);

create table if not exists colombia.award
(
    ocid                             text,
    data_id                          bigint,
    award_id                         text,
    award_description                text,
    award_title                      text,
    date                             timestamp,
    contract_period_start_date       timestamp,
    contract_period_duration_in_days numeric,
    amount                           numeric,
    currency                         text,
    status                           text,
    supplier_id                      text,
    supplier_name                    text,
    id                               bigserial not null
        constraint award_pk
            primary key
);

create table if not exists colombia.contract
(
    ocid             text,
    data_id          bigint,
    contract_id      text,
    award_id         text,
    description      text,
    date_signed      timestamp,
    amount           numeric,
    currency         text,
    status           text,
    duration_in_days numeric,
    start_date       timestamp,
    end_date         timestamp,
    transactions     jsonb,
    amendments       jsonb,
    id               bigserial not null
        constraint contract_pk
            primary key
);

create table if not exists colombia.tender_items
(
    ocid                       text,
    data_id                    bigint,
    item_id                    text,
    description                text,
    classification_id          text,
    classification_description text,
    id                         bigserial not null
        constraint tender_items_pk
            primary key
);

create table if not exists colombia.award_items
(
    ocid                       text,
    data_id                    bigint,
    award_id                   text,
    item_id                    text,
    description                text,
    classification_id          text,
    classification_description text,
    quantity                   numeric,
    unit_name                  text,
    id                         bigserial not null
        constraint award_items_pk
            primary key
);

create table if not exists colombia.contract_items
(
    ocid                       text,
    data_id                    bigint,
    contract_id                text,
    item_id                    text,
    description                text,
    classification_id          text,
    classification_description text,
    quantity                   numeric,
    unit_name                  text,
    id                         bigserial not null
        constraint contract_items_pk
            primary key
);

create table if not exists colombia.notification
(
    id                bigserial not null,
    ocid              text      not null,
    characteristics   jsonb,
    status            varchar default 'PENDING'::character varying,
    rules             jsonb,
    event             text,
    tweet_id          text,
    parent_id         bigint,
    creation_date     timestamp,
    notification_date timestamp
);

ALTER TABLE colombia.notification ALTER COLUMN creation_date SET DEFAULT now();
