# https://docs.scrapy.org/en/latest/topics/item-pipeline.html
# https://docs.scrapy.org/en/latest/topics/signals.html#item-signals
import json
import pkgutil

import dataset
import jsonpointer
from jsonschema import FormatChecker
from jsonschema.validators import Draft4Validator, RefResolver
from scrapy.exceptions import DropItem

from analysis.analysis import classify
from kingfisher_scrapy.items import File, FileItem, PluckedItem, ReleaseDataItem
from transform.transform import transform


def _json_loads(basename):
    return json.loads(pkgutil.get_data('kingfisher_scrapy', f'item_schema/{basename}.json'))


class EmpatiaETLPipeline(object):
    def __init__(self, **kwargs):
        self.args = kwargs

    @classmethod
    def from_crawler(cls, crawler):
        args = crawler.settings.get('PG_PIPELINE', {})
        return cls(**args)

    def close_spider(self, spider):
        transform(self.db, spider.schema, spider.rules)
        classify(self.db, spider.schema)

    def open_spider(self, spider):
        if self.args.get('connection'):
            self.db = dataset.connect(self.args.get('connection'), schema=spider.schema)
            self.table = self.db[self.args.get('table_name')]
            self.pkey = self.args.get('pkey')
            self.types = self.args.get('types', {})
            self.ignore_identical = self.args.get('ignore_identical')
            self.table.create_index([self.pkey])
            self.table.create_index(self.ignore_identical)
            self.onconflict = self.args.get('onconflict', 'ignore')

    def process_item(self, item, spider):
        if not isinstance(item, File):
            return item
        for release in _get_releases_data(item):
            if self.onconflict == 'ignore':
                self.table.insert_ignore(
                    release, self.ignore_identical, types=self.types)
            elif self.onconflict == 'upsert':
                self.table.upsert(
                    release, self.ignore_identical, types=self.types)
            elif self.onconflict == 'non-null':
                row, res = self.table._upsert_pre_check(
                    release, self.ignore_identical, None)
                selected = release
                if res is not None:
                    # remove keys with none value
                    selected = dict((k, v) for k, v in release.iteritems() if v)
                    self.table.upsert(
                        selected, self.ignore_identical, types=self.types)
                else:
                    self.table.insert(
                        selected, self.ignore_identical, types=self.types)
            else:
                raise Exception("no such strategy: %s" % (self.onconflict))
        return item


def _get_releases_data(item):
    releases = []
    data = json.loads(item['data'])
    if item['data_type'] == 'record_package':
        for record in data['records']:
            releases.append(ReleaseDataItem({
                'data': record['compiledRelease'],
                'release_id': record['compiledRelease']['id'],
                'ocid': record['compiledRelease']['ocid']
            }))
    if item['data_type'] == 'release_package':
        for release in data['releases']:
            releases.append(ReleaseDataItem({
                'data': release,
                'release_id': release['id'],
                'ocid': release['ocid']
            }))
    return releases


class Validate:
    def __init__(self):
        self.validators = {}
        self.files = set()
        self.file_items = set()

        resolver = RefResolver.from_schema(_json_loads('item'))
        checker = FormatChecker()
        for item in ('File', 'FileError', 'FileItem'):
            self.validators[item] = Draft4Validator(_json_loads(item), resolver=resolver, format_checker=checker)

    def process_item(self, item, spider):
        if hasattr(item, 'validate'):
            self.validators.get(item.__class__.__name__).validate(dict(item))

        if isinstance(item, FileItem):
            key = (item['file_name'], item['number'])
            if key in self.file_items:
                spider.logger.warning('Duplicate FileItem: {!r}'.format(key))
            self.file_items.add(key)
        elif isinstance(item, File):
            key = item['file_name']
            if key in self.files:
                spider.logger.warning('Duplicate File: {!r}'.format(key))
            self.files.add(key)

        return item


class Pluck:
    def __init__(self):
        self.processed = set()

    def process_item(self, item, spider):
        # Skip this pipeline stage unless explicitly requested.
        if not spider.pluck:
            return item

        # Drop any extra items that are yielded before the spider closes.
        if spider.name in self.processed:
            spider.crawler.engine.close_spider(spider, reason='processed')
            raise DropItem()

        # Drop FileError items, so that we keep trying to get data.
        if not isinstance(item, (File, FileItem)):
            raise DropItem()

        value = None

        if spider.package_pointer:
            try:
                package = _get_package(item)
            except NotImplementedError as e:
                value = f'error: {str(e)}'
            else:
                value = _resolve_pointer(package, spider.package_pointer)
        else:  # spider.release_pointer
            if item['data_type'] in ('release_package', 'release_package_list', 'release_package_list_in_results',
                                     'release_list', 'release', 'compiled_release'):
                data = _get_releases(item)
                if data:
                    value = max(_resolve_pointer(r, spider.release_pointer) for r in data)
            elif item['data_type'] in ('record_package', 'record_package_list', 'record_package_list_in_results',
                                       'record_list', 'record'):
                data = _get_records(item)
                if data:
                    # This assumes that the first record in the record package has the desired value.
                    data = data[0]
                    if 'releases' in data:
                        value = max(_resolve_pointer(r, spider.release_pointer) for r in data['releases'])
                    elif 'compiledRelease' in data:
                        value = _resolve_pointer(data['compiledRelease'], spider.release_pointer)

        self.processed.add(spider.name)

        if value and spider.truncate:
            value = value[:spider.truncate]

        return PluckedItem({'value': value})


def _resolve_pointer(data, pointer):
    try:
        return jsonpointer.resolve_pointer(data, pointer)
    except jsonpointer.JsonPointerException:
        return f'error: {pointer} not found'


def _get_package(item):
    data = json.loads(item['data'])

    if item['data_type'] in ('release_package', 'record_package'):
        return data
    # This assumes that the first package in the list has the desired value.
    elif item['data_type'] in ('release_package_list', 'record_package_list'):
        return data[0]
    elif item['data_type'] in ('release_package_list_in_results', 'record_package_list_in_results'):
        return data['results'][0]

    raise NotImplementedError(f"no package for data_type: {item['data_type']}")


def _get_releases(item):
    data = json.loads(item['data'])

    if item['data_type'] == 'release_package':
        return data['releases']
    # This assumes that the first package in the list has the desired value.
    elif item['data_type'] == 'release_package_list':
        return data[0]['releases']
    elif item['data_type'] == 'release_package_list_in_results':
        return data['results'][0]['releases']
    elif item['data_type'] == 'release_list':
        return data
    elif item['data_type'] in ('release', 'compiled_release'):
        return [data]

    raise NotImplementedError(f"unhandled data_type: {item['data_type']}")


def _get_records(item):
    data = json.loads(item['data'])

    if item['data_type'] == 'record_package':
        return data['records']
    # This assumes that the first package in the list has the desired value.
    elif item['data_type'] == 'record_package_list':
        return data[0]['records']
    elif item['data_type'] == 'record_package_list_in_results':
        return data['results'][0]['records']
    elif item['data_type'] == 'record_list':
        return data
    elif item['data_type'] == 'record':
        return [data]

    raise NotImplementedError(f"unhandled data_type: {item['data_type']}")
